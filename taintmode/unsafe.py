# To run it:
# python unsafe.py <email address> <filename>
#
#
# For instance,
# python ale@chalmers.se file.txt
#
# You should see:
#
# Running ----> ./mail -s "Re: your file" ale@chalmers.se < file.txt
# The mail has been sent!

import sys
import os

usermail = sys.argv[1]
file = sys.argv[2]


cmd = './mail -s "Re: your file" ' + usermail + ' < ' + file
print 'Running ----> ' + cmd
os.system(cmd)

# Possible attack:
#
# python unsafe.py ale@chalmers.se 'file.txt ; rm -f /'
#
# Don't try this example above, it is to show that you can
# inject code after the ;. Instead, try this:
#
# python unsafe.py ale@domain.se 'file.txt ; echo attack'
